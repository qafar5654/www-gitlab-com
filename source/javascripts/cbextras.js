!function(){

  var loopcount;
  var CBNCMessage  = '<div class="cbnc-message">Trouble viewing this resource? You may need to update your <a href="javascript:Cookiebot.show();">cookie settings</a> to allow <strong>personalization</strong> cookies.</div>';
  var renderStandardMessage = function(loopcount, targets)
  {
    var wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'cbneedsconsent');
    targets[loopcount].parentNode.insertBefore(wrapper, targets[loopcount]);
    wrapper.appendChild(targets[loopcount]);
    wrapper.insertAdjacentHTML('afterbegin', CBNCMessage);
  }

  var targets=document.querySelectorAll('iframe[data-src*="youtube"]');
  for(loopcount=0;loopcount<targets.length;loopcount++)
  {
    renderStandardMessage(loopcount, targets);
  }

  var targets=document.querySelectorAll('iframe[data-src*="calendar"]');
  for(loopcount=0;loopcount<targets.length;loopcount++)
  {
    renderStandardMessage(loopcount, targets);
  }

}();